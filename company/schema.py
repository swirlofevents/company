import graphene
from jobs_list.schema import Query as queryes_list
from jobs_list.schema import Mutation as mutation_list


class Query(queryes_list, graphene.ObjectType):
    pass

class Mutation(mutation_list, graphene.ObjectType):
    pass

schema = graphene.Schema(query = Query, mutation = Mutation)