from django.db import models


#Сущность Заказы
class Order(models.Model):
    name = models.CharField("Наименование заказа", max_length=255)
    version = models.IntegerField("Версия заказа", default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def orderArchive(self, order):

        obj = OrderArchive(
            identifier_order=order,
            name=self.name, 
            version=self.version,
            )
        obj.save()
        return obj

# Архив заказов
class OrderArchive(models.Model):
    identifier_order = models.ForeignKey(Order, related_name='previous_versions', on_delete=models.SET_NULL, null=True)
    name = models.CharField("Наименование заказа", max_length=255)
    version = models.IntegerField("Версия заказа", default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Версия архива"
        verbose_name_plural = "Версии архивов"

#Сущность Работы, которые входят в заказ
class Job(models.Model):
    name = models.CharField("Наименование работы", max_length=255)
    order_pk = models.ForeignKey(Order, related_name = 'jobs', on_delete=models.CASCADE)
    date_start = models.DateField("Дата начала работы")
    date_end = models.DateField("Дата окончания работы")
    order_version = models.IntegerField("Версия работы заказа", default=0)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('date_start', 'name')
        verbose_name = "Работа"
        verbose_name_plural = "Работы"

    def jobsArchive(self, order, orderArchive):
        """Сохранение предыдущих версии"""
        for job in Job.objects.filter(order_pk=order.pk):
            previous_version = JobArchive(
                identifier_job = job,
                name = job.name,
                order_pk = orderArchive,
                date_start = job.date_start,
                date_end = job.date_end,                    
                order_version = job.order_version
            )
            job.order_version = order.version
            previous_version.save()
            job.save()
        return None

# Архив работ
class JobArchive(models.Model):
    identifier_job = models.ForeignKey(Job, related_name = 'previous_versions', on_delete=models.SET_NULL, null=True)
    name = models.CharField("Наименование работы", max_length=255)
    order_pk = models.ForeignKey(OrderArchive, related_name = 'jobs', on_delete=models.SET_NULL, null=True)
    date_start = models.DateField("Дата начала работы")
    date_end = models.DateField("Дата окончания работы")
    order_version = models.IntegerField("Версия работы заказа", default=0)

    class Meta:
        ordering = ('name', 'order_version')
        verbose_name = "Версия работы"
        verbose_name_plural = "Версии работ"


#Сущность Рабочие, которые выполняют работу
class Worker(models.Model):
    full_name = models.CharField("ФИО", max_length=255)

    def __str__(self):
        return self.full_name
    
    class Meta:
        verbose_name = "Рабочий"
        verbose_name_plural = "Рабочие"

#Сущность Рабочий-работа, ассоциативная сущность  
class Worker_job(models.Model):
    worker_pk = models.ForeignKey(Worker, related_name='workers_job', on_delete=models.CASCADE)
    job_pk = models.ForeignKey(Job, related_name='jobs_worker', on_delete=models.CASCADE)
