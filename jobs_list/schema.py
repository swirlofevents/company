import graphene
from .models import *
from graphene_django.types import DjangoObjectType, ObjectType
from django.core.exceptions import ObjectDoesNotExist


class JobType(DjangoObjectType):

    class Meta:
        model = Job

class JobArchiveType(DjangoObjectType):

    class Meta:
        model = JobArchive


class OrderType(DjangoObjectType):


    class Meta:
        model = Order

class OrderArchiveType(DjangoObjectType):

    class Meta:
        model = OrderArchive


class WorkerType(DjangoObjectType):

    class Meta:
        model = Worker

class WorkerJobType(DjangoObjectType):

    class Meta:
        model = Worker_job


### Inputs

class JobInput(graphene.InputObjectType):

    name = graphene.String()
    date_start = graphene.Date()
    date_end = graphene.Date()      


class OrderInput(graphene.InputObjectType):

    name = graphene.String()
    job = graphene.List(JobInput)


class WorkerInput(graphene.InputObjectType):

    full_name = graphene.String()


class WorkerJobInput(graphene.InputObjectType):

    worker_pk = graphene.Int()
    job_pk = graphene.Int()


class JobArchiveInput(graphene.InputObjectType):

    identifier_job = graphene.Int()
    name = graphene.String()
    date_start = graphene.Date()
    date_end = graphene.Date()  


### Job's classes


# Create job
class CreateJob(graphene.Mutation):
    
    class Arguments:
        input = JobInput(required=True)
        order_pk = graphene.Int(required=True)
    
    status = graphene.Boolean()
    job = graphene.Field(JobType)

    @staticmethod
    def mutate(root, info, order_pk, input=None):
        try:
            order_obj = Order.objects.get(pk=order_pk)
            job_instance = Job(
                name=input.name, 
                date_start=input.date_start, 
                date_end=input.date_end, 
                order_pk=order_obj
            )
            job_instance.save()
            status=True
        except ObjectDoesNotExist as Exception:
            raise Exception
        
        return CreateJob(status=status, job=job_instance)


# Update job
class UpdateJob(graphene.Mutation):

    class Arguments:
        pk = graphene.Int(required=True)
        input = JobInput(required=True)
    
    status = graphene.Boolean()
    job = graphene.Field(JobType)

    @staticmethod
    def mutate(root, info, pk, input=None):
        try:
            if input is not None:
                job_instance = Job.objects.get(pk=pk)
                order = Order.objects.get(pk=job_instance.order_pk.pk)
                orderArchive = order.orderArchive(order)
                order.version += 1
                order.save()
                job_instance.jobsArchive(order, orderArchive)
                status = True
                if input.name is not None:
                    job_instance.name = input.name
                if input.date_start is not None:
                    job_instance.date_start = input.date_start
                if input.date_end is not None:
                    job_instance.date_end = input.date_end
                job_instance.order_version = order.version
                job_instance.save()
            status = False
            return UpdateJob(status=status, job=job_instance)
        except ObjectDoesNotExist as Exception:
            raise Exception


# Delete job
class DeleteJob(graphene.Mutation):

    class Arguments:
        pk = graphene.Int(required=True)
    
    status = graphene.Boolean()

    @staticmethod
    def mutate(root, info, pk, **kwargs):
        status = False
        try:
            job_instance = Job.objects.get(pk=pk)
            order = Order.objects.get(pk=job_instance.order_pk.pk)
            orderArchive = order.orderArchive(order)
            order.version += 1
            order.save()
            job_instance.jobsArchive(order, orderArchive)
            job_instance.delete()
            status = True
            return DeleteOrder(status=status)
        except ObjectDoesNotExist as Exception:
            raise Exception


### Order's classes


# Create order
class CreateOrder(graphene.Mutation):

    class Arguments:
        input = OrderInput(required=True)

    status = graphene.Boolean()
    order = graphene.Field(OrderType)

    @staticmethod
    def mutate(root, info, input=None):
        order_instance = Order(name=input.name)
        order_instance.save()
        if input.job is not None:
            for job in input.job:
                job_instance = Job(
                    name=job.name, 
                    date_start=job.date_start, 
                    date_end=job.date_end, 
                    order_pk=order_instance
                    )
                job_instance.save()
        status = True
        return CreateOrder(status=status, order=order_instance)


# Update Order
class UpdateOrder(graphene.Mutation):

    class Arguments:
        pk = graphene.Int(required=True)
        input = OrderInput(required=True)

    status = graphene.Boolean()
    order = graphene.Field(OrderType)

    @staticmethod
    def mutate(root, info, pk, input=None):
        status = False
        order_instance = Order.objects.get(pk=pk)
        if order_instance:
            order_instance.name = input.name
            order_instance.save()
            status = True
            return UpdateOrder(status=status, order=order_instance)
        return UpdateOrder(status=status, order=None)


# Delete Order
class DeleteOrder(graphene.Mutation):

    class Arguments:
        pk = graphene.Int(required=True)
    
    status = graphene.Boolean()

    @staticmethod
    def mutate(root, info, pk, **kwargs):
        status = False
        try:
            obj = Order.objects.get(pk=pk)
            obj.delete()
            status = True
            return DeleteOrder(status=status)
        except ObjectDoesNotExist as Exception:
            raise Exception


### Worker's classes


class CreateWorker(graphene.Mutation):

    class Arguments:
        full_name = graphene.String(required=True)
    
    status = graphene.Boolean()
    worker = graphene.Field(WorkerType)

    @staticmethod
    def mutate(root, info, full_name=None, **kwargs):
        worker_instance = Worker(full_name=full_name)
        worker_instance.save()
        status = True
        return CreateWorker(status=status, worker=worker_instance)


class UpdateWorker(graphene.Mutation):

    class Arguments:
        pk = graphene.Int(required=True)
        full_name = graphene.String(required=True)

    status = graphene.Boolean()
    worker = graphene.Field(WorkerType)

    @staticmethod
    def mutate(root, info, pk, full_name):
        try:
            worker_instance = Worker.objects.get(pk=pk)
            worker_instance.full_name = full_name
            worker_instance.save()
            status = True
            return UpdateWorker(status=status, worker=worker_instance)
        except ObjectDoesNotExist as Exception:
            raise Exception


class DeleteWorker(graphene.Mutation):

    class Arguments:
        pk = graphene.Int(required=True)

    status = graphene.Boolean()

    @staticmethod
    def mutate(root, info, pk, **kwargs):
        try:
            obj = Worker.objects.get(pk=pk)
            obj.delete()
            staus = True
            return DeleteWorker(staus=staus)
        except ObjectDoesNotExist as Exception:
            raise Exception


### Worker's jobs classes


class CreateWorkerJob(graphene.Mutation):

    class Arguments:
        input = WorkerJobInput(required=True)

    status = graphene.Boolean()
    worker_job = graphene.Field(WorkerJobType)

    @staticmethod
    def mutate(root, info, input, **kwargs):
        try:
            worker = Worker.objects.get(pk=input.worker_pk)
            job = Job.objects.get(pk=input.job_pk)
            worker_job_instance = Worker_job(worker_pk=worker, job_pk=job)
            worker_job_instance.save()
            status = True
        except ObjectDoesNotExist as Exception:
            raise Exception
        return CreateWorkerJob(status=status, worker_job=worker_job_instance)


class UpdateWorkerJob(graphene.Mutation):

    class Arguments:
        input = WorkerJobInput(required=True)
        new_worker = graphene.Int(required=True)

    status = graphene.Boolean()
    worker_job = graphene.Field(WorkerJobType)

    @staticmethod
    def mutate(root, info, input, new_worker, **kwargs):
        try:
            worker_job_instance = Worker_job.objects.get(
                worker_pk=input.worker_pk,
                job_pk=input.job_pk
                )
            worker = Worker.objects.get(pk=new_worker)
            worker_job_instance.worker_pk = worker
            worker_job_instance.save()
            status = True
        except ObjectDoesNotExist as Exception:
            raise Exception
        return UpdateWorkerJob(status=status, worker_job=worker_job_instance)


class DeleteWorkerJob(graphene.Mutation):

    class Arguments:
        input = WorkerJobInput(required=True)

    status = graphene.Boolean()

    @staticmethod
    def mutate(root, info, input, **kwargs):
        try:
            worker_job_instance = Worker_job.objects.get(
                worker_pk=input.worker_pk,
                job_pk=input.job_pk
                )
            worker_job_instance.delete()
            status = True
        except ObjectDoesNotExist as Exception:
            raise Exception
        return UpdateWorkerJob(status=status)


### Queries
class Query(graphene.ObjectType): 

    orders_list  = graphene.List(OrderType)
    order = graphene.Field(OrderType, pk = graphene.Int())
    order_version = graphene.Field(OrderArchiveType, pk=graphene.Int(), version=graphene.Int())
    jobs_list = graphene.List(JobType)
    job = graphene.Field(JobType, pk=graphene.Int())
    worker_list = graphene.List(WorkerType)
    worker = graphene.Field(WorkerType, pk=graphene.Int())

    # Order
    def resolve_order(self, info, **kwargs):
        pk = kwargs.get('pk')

        if pk is not None:  
            try:
                return Order.objects.get(pk=pk)
            except ObjectDoesNotExist as Exception:
                raise Exception
        return None

    def resolve_order_version(self, info, **kwargs):
        pk = kwargs.get('pk')
        version = kwargs.get('version')

        if pk and version is not None:
            try:
                return OrderArchive.objects.get(identifier_order=pk, version=version)
            except ObjectDoesNotExist as Exception:
                raise Exception
        return None    

    def resolve_orders_list(self, info, **kwargs):
        return Order.objects.all()
    
    # Job
    def resolve_job(self, info, **kwargs):
        pk = kwargs.get('pk')

        if pk is not None:  
            try:
                return Job.objects.get(pk=pk)
            except ObjectDoesNotExist as Exception:
                raise Exception
        return None

    def resolve_jobs_list(self, info, **kwargs):
        return Job.objects.all()
    
    # Worker
    def resolve_worker(self, info, **kwargs):
        pk = kwargs.get('pk')

        if pk is not None:
            try:
                return Worker.objects.get(pk=pk)
            except ObjectDoesNotExist as Exception:
                raise Exception
        return None

    def resolve_worker_list(self, info, **kwargs):
        return Worker.objects.all()


### Mutations
class Mutation(graphene.ObjectType):

    create_order = CreateOrder.Field()
    update_order = UpdateOrder.Field()
    delete_order = DeleteOrder.Field()

    create_job = CreateJob.Field()
    update_job = UpdateJob.Field()
    delete_job = DeleteJob.Field()

    create_worker = CreateWorker.Field()
    update_worker = UpdateWorker.Field()
    delete_worker = DeleteWorker.Field()

    create_worker_job = CreateWorkerJob.Field()
    update_worker_job = UpdateWorkerJob.Field()
    delete_worker_job = DeleteWorkerJob.Field()