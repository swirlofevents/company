from django.contrib import admin
from .models import *

admin.site.register(Order)
admin.site.register(Job)
admin.site.register(Worker)
admin.site.register(Worker_job)
